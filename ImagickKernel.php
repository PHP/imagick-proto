<?php

class ImagickKernel {
    /* Methods */
    public function addKernel(ImagickKernel $ImagickKernel): void { }
    public function addUnityKernel(float $scale): void { }
    public static function fromBuiltin(int $kernelType, string $kernelString): ImagickKernel { return new ImagickKernel(); }
    public static function fromMatrix(array $matrix, array $origin = null): ImagickKernel { return new ImagickKernel(); }
    public function getMatrix(): array { return []; }
    public function scale(float $scale, int $normalizeFlag = null): void { }
    public function separate(): array { return []; }
}
