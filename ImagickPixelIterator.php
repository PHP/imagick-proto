<?php

class ImagickPixelIterator {
    public function clear(): bool { return false; }
    public function __construct(Imagick $wand) { }
    public function destroy(): bool { return false; }
    public function getCurrentIteratorRow(): array { return []; }
    public function getIteratorRow(): int { return 0; }
    public function getNextIteratorRow(): array { return []; }
    public function getPreviousIteratorRow(): array { return []; }
    public function newPixelIterator(Imagick $wand): bool { return false; }

    public function newPixelRegionIterator(
        Imagick $wand,
        int $x,
        int $y,
        int $columns,
        int $rows
    ): bool { return false; }

    public function resetIterator(): bool { return false; }
    public function setIteratorFirstRow(): bool { return false; }
    public function setIteratorLastRow(): bool { return false; }
    public function setIteratorRow(int $row): bool { return false; }
    public function syncIterator(): bool { return false; }
}
