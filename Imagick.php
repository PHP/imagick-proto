<?php

class Imagick implements Iterator {
    const ALIGN_CENTER = 2;
    const ALIGN_LEFT = 1;
    const ALIGN_RIGHT = 3;
    const ALIGN_UNDEFINED = 0;
    const ALPHACHANNEL_ACTIVATE = 1;
    const ALPHACHANNEL_ASSOCIATE = 2;
    const ALPHACHANNEL_BACKGROUND = 3;
    const ALPHACHANNEL_COPY = 4;
    const ALPHACHANNEL_DEACTIVATE = 5;
    const ALPHACHANNEL_DISCRETE = 6;
    const ALPHACHANNEL_DISSOCIATE = 7;
    const ALPHACHANNEL_EXTRACT = 8;
    const ALPHACHANNEL_OFF = 9;
    const ALPHACHANNEL_ON = 10;
    const ALPHACHANNEL_OPAQUE = 11;
    const ALPHACHANNEL_REMOVE = 12;
    const ALPHACHANNEL_SET = 13;
    const ALPHACHANNEL_SHAPE = 14;
    const ALPHACHANNEL_TRANSPARENT = 15;
    const ALPHACHANNEL_UNDEFINED = 0;
    const AUTO_THRESHOLD_KAPUR = 1;
    const AUTO_THRESHOLD_OTSU = 2;
    const AUTO_THRESHOLD_TRIANGLE = 3;
    const CHANNEL_ALL = 134217727;
    const CHANNEL_ALPHA = 16;
    const CHANNEL_BLACK = 8;
    const CHANNEL_BLUE = 4;
    const CHANNEL_COMPOSITES = 31;
    const CHANNEL_COMPOSITE_MASK = 512;
    const CHANNEL_CYAN = 1;
    const CHANNEL_DEFAULT = 134217727;
    const CHANNEL_GRAY = 1;
    const CHANNEL_GRAY_CHANNELS = 1024;
    const CHANNEL_GREEN = 2;
    const CHANNEL_INDEX = 32;
    const CHANNEL_MAGENTA = 2;
    const CHANNEL_META = 256;
    const CHANNEL_OPACITY = 16;
    const CHANNEL_READ_MASK = 64;
    const CHANNEL_RED = 1;
    const CHANNEL_RGBA = 23;
    const CHANNEL_RGBS = 512;
    const CHANNEL_SYNC = 131072;
    const CHANNEL_TRUEALPHA = 256;
    const CHANNEL_UNDEFINED = 0;
    const CHANNEL_WRITE_MASK = 128;
    const CHANNEL_YELLOW = 4;
    const COLORSPACE_ADOBE98 = 36;
    const COLORSPACE_CMY = 1;
    const COLORSPACE_CMYK = 2;
    const COLORSPACE_DISPLAYP3 = 35;
    const COLORSPACE_GRAY = 3;
    const COLORSPACE_HCL = 4;
    const COLORSPACE_HCLP = 5;
    const COLORSPACE_HSB = 6;
    const COLORSPACE_HSI = 7;
    const COLORSPACE_HSL = 8;
    const COLORSPACE_HSV = 9;
    const COLORSPACE_HWB = 10;
    const COLORSPACE_JZAZBZ = 34;
    const COLORSPACE_LCH = 12;
    const COLORSPACE_LCHAB = 13;
    const COLORSPACE_LCHUV = 14;
    const COLORSPACE_LINEARGRAY = 33;
    const COLORSPACE_LMS = 16;
    const COLORSPACE_LOG = 15;
    const COLORSPACE_LUV = 17;
    const COLORSPACE_OHTA = 18;
    const COLORSPACE_PROPHOTO = 37;
    const COLORSPACE_REC601YCBCR = 19;
    const COLORSPACE_REC709YCBCR = 20;
    const COLORSPACE_RGB = 21;
    const COLORSPACE_SCRGB = 22;
    const COLORSPACE_SRGB = 23;
    const COLORSPACE_TRANSPARENT = 24;
    const COLORSPACE_UNDEFINED = 0;
    const COLORSPACE_XYY = 25;
    const COLORSPACE_XYZ = 26;
    const COLORSPACE_YCBCR = 27;
    const COLORSPACE_YCC = 28;
    const COLORSPACE_YDBDR = 29;
    const COLORSPACE_YIQ = 30;
    const COLORSPACE_YPBPR = 31;
    const COLORSPACE_YUV = 32;
    const COLOR_ALPHA = 18;
    const COLOR_BLACK = 11;
    const COLOR_BLUE = 12;
    const COLOR_CYAN = 13;
    const COLOR_FUZZ = 19;
    const COLOR_GREEN = 14;
    const COLOR_MAGENTA = 17;
    const COLOR_RED = 15;
    const COLOR_YELLOW = 16;
    const COMPLEX_OPERATOR_ADD = 1;
    const COMPLEX_OPERATOR_CONJUGATE = 2;
    const COMPLEX_OPERATOR_DIVIDE = 3;
    const COMPLEX_OPERATOR_MAGNITUDEPHASE = 4;
    const COMPLEX_OPERATOR_MULTIPLY = 5;
    const COMPLEX_OPERATOR_REALIMAGINARY = 6;
    const COMPLEX_OPERATOR_SUBTRACT = 7;
    const COMPOSITE_ATOP = 2;
    const COMPOSITE_BLEND = 3;
    const COMPOSITE_BLUR = 4;
    const COMPOSITE_BUMPMAP = 5;
    const COMPOSITE_CHANGEMASK = 6;
    const COMPOSITE_CLEAR = 7;
    const COMPOSITE_COLORBURN = 8;
    const COMPOSITE_COLORDODGE = 9;
    const COMPOSITE_COLORIZE = 10;
    const COMPOSITE_COPY = 13;
    const COMPOSITE_COPYALPHA = 17;
    const COMPOSITE_COPYBLACK = 11;
    const COMPOSITE_COPYBLUE = 12;
    const COMPOSITE_COPYCYAN = 14;
    const COMPOSITE_COPYGREEN = 15;
    const COMPOSITE_COPYMAGENTA = 16;
    const COMPOSITE_COPYOPACITY = 17;
    const COMPOSITE_COPYRED = 18;
    const COMPOSITE_COPYYELLOW = 19;
    const COMPOSITE_DARKEN = 20;
    const COMPOSITE_DARKENINTENSITY = 21;
    const COMPOSITE_DEFAULT = 54;
    const COMPOSITE_DIFFERENCE = 22;
    const COMPOSITE_DISPLACE = 23;
    const COMPOSITE_DISSOLVE = 24;
    const COMPOSITE_DISTORT = 25;
    const COMPOSITE_DIVIDEDST = 26;
    const COMPOSITE_DIVIDESRC = 27;
    const COMPOSITE_DST = 29;
    const COMPOSITE_DSTATOP = 28;
    const COMPOSITE_DSTIN = 30;
    const COMPOSITE_DSTOUT = 31;
    const COMPOSITE_DSTOVER = 32;
    const COMPOSITE_EXCLUSION = 33;
    const COMPOSITE_FREEZE = 72;
    const COMPOSITE_HARDLIGHT = 34;
    const COMPOSITE_HARDMIX = 35;
    const COMPOSITE_HUE = 36;
    const COMPOSITE_IN = 37;
    const COMPOSITE_INTERPOLATE = 73;
    const COMPOSITE_LIGHTEN = 39;
    const COMPOSITE_LIGHTENINTENSITY = 40;
    const COMPOSITE_LINEARBURN = 41;
    const COMPOSITE_LINEARDODGE = 42;
    const COMPOSITE_LINEARLIGHT = 43;
    const COMPOSITE_LUMINIZE = 44;
    const COMPOSITE_MATHEMATICS = 45;
    const COMPOSITE_MINUSDST = 46;
    const COMPOSITE_MINUSSRC = 47;
    const COMPOSITE_MODULATE = 48;
    const COMPOSITE_MODULUSADD = 49;
    const COMPOSITE_MODULUSSUBTRACT = 50;
    const COMPOSITE_MULTIPLY = 51;
    const COMPOSITE_NEGATE = 74;
    const COMPOSITE_NO = 52;
    const COMPOSITE_OUT = 53;
    const COMPOSITE_OVER = 54;
    const COMPOSITE_OVERLAY = 55;
    const COMPOSITE_PEGTOPLIGHT = 56;
    const COMPOSITE_PINLIGHT = 57;
    const COMPOSITE_PLUS = 58;
    const COMPOSITE_REFLECT = 75;
    const COMPOSITE_REPLACE = 59;
    const COMPOSITE_RMSE = 79;
    const COMPOSITE_SATURATE = 60;
    const COMPOSITE_SCREEN = 61;
    const COMPOSITE_SOFTBURN = 76;
    const COMPOSITE_SOFTDODGE = 77;
    const COMPOSITE_SOFTLIGHT = 62;
    const COMPOSITE_SRC = 64;
    const COMPOSITE_SRCATOP = 63;
    const COMPOSITE_SRCIN = 65;
    const COMPOSITE_SRCOUT = 66;
    const COMPOSITE_SRCOVER = 67;
    const COMPOSITE_STAMP = 78;
    const COMPOSITE_STEREO = 71;
    const COMPOSITE_THRESHOLD = 68;
    const COMPOSITE_UNDEFINED = 0;
    const COMPOSITE_VIVIDLIGHT = 69;
    const COMPOSITE_XOR = 70;
    const COMPRESSION_B44 = 2;
    const COMPRESSION_B44A = 1;
    const COMPRESSION_BZIP = 3;
    const COMPRESSION_DWAA = 24;
    const COMPRESSION_DWAB = 25;
    const COMPRESSION_DXT1 = 4;
    const COMPRESSION_DXT3 = 5;
    const COMPRESSION_DXT5 = 6;
    const COMPRESSION_FAX = 7;
    const COMPRESSION_GROUP4 = 8;
    const COMPRESSION_JBIG1 = 9;
    const COMPRESSION_JBIG2 = 10;
    const COMPRESSION_JPEG = 12;
    const COMPRESSION_JPEG2000 = 11;
    const COMPRESSION_LOSSLESSJPEG = 13;
    const COMPRESSION_LZMA = 14;
    const COMPRESSION_LZW = 15;
    const COMPRESSION_NO = 16;
    const COMPRESSION_PIZ = 17;
    const COMPRESSION_PXR24 = 18;
    const COMPRESSION_RLE = 19;
    const COMPRESSION_UNDEFINED = 0;
    const COMPRESSION_WEBP = 23;
    const COMPRESSION_ZIP = 20;
    const COMPRESSION_ZIPS = 21;
    const COMPRESSION_ZSTD = 22;
    const DECORATION_LINETHROUGH = 4;
    const DECORATION_LINETROUGH = 4;
    const DECORATION_NO = 1;
    const DECORATION_OVERLINE = 3;
    const DECORATION_UNDERLINE = 2;
    const DIRECTION_LEFT_TO_RIGHT = 2;
    const DIRECTION_RIGHT_TO_LEFT = 1;
    const DISPOSE_BACKGROUND = 2;
    const DISPOSE_NONE = 1;
    const DISPOSE_PREVIOUS = 3;
    const DISPOSE_UNDEFINED = 0;
    const DISPOSE_UNRECOGNIZED = 0;
    const DISTORTION_AFFINE = 1;
    const DISTORTION_AFFINEPROJECTION = 2;
    const DISTORTION_ARC = 9;
    const DISTORTION_BARREL = 14;
    const DISTORTION_BARRELINVERSE = 15;
    const DISTORTION_BILINEAR = 6;
    const DISTORTION_BILINEARFORWARD = 6;
    const DISTORTION_BILINEARREVERSE = 7;
    const DISTORTION_CYLINDER2PLANE = 12;
    const DISTORTION_DEPOLAR = 11;
    const DISTORTION_PERSPECTIVE = 4;
    const DISTORTION_PERSPECTIVEPROJECTION = 5;
    const DISTORTION_PLANE2CYLINDER = 13;
    const DISTORTION_POLAR = 10;
    const DISTORTION_POLYNOMIAL = 8;
    const DISTORTION_RESIZE = 17;
    const DISTORTION_RIGID_AFFINE = 19;
    const DISTORTION_SCALEROTATETRANSLATE = 3;
    const DISTORTION_SENTINEL = 18;
    const DISTORTION_SHEPARDS = 16;
    const DISTORTION_UNDEFINED = 0;
    const DITHERMETHOD_FLOYDSTEINBERG = 3;
    const DITHERMETHOD_NO = 1;
    const DITHERMETHOD_RIEMERSMA = 2;
    const DITHERMETHOD_UNDEFINED = 0;
    const EVALUATE_ABS = 1;
    const EVALUATE_ADD = 2;
    const EVALUATE_ADDMODULUS = 3;
    const EVALUATE_AND = 4;
    const EVALUATE_COSINE = 5;
    const EVALUATE_DIVIDE = 6;
    const EVALUATE_EXPONENTIAL = 7;
    const EVALUATE_GAUSSIANNOISE = 8;
    const EVALUATE_IMPULSENOISE = 9;
    const EVALUATE_INVERSE_LOG = 33;
    const EVALUATE_LAPLACIANNOISE = 10;
    const EVALUATE_LEFTSHIFT = 11;
    const EVALUATE_LOG = 12;
    const EVALUATE_MAX = 13;
    const EVALUATE_MEAN = 14;
    const EVALUATE_MEDIAN = 15;
    const EVALUATE_MIN = 16;
    const EVALUATE_MULTIPLICATIVENOISE = 17;
    const EVALUATE_MULTIPLY = 18;
    const EVALUATE_OR = 19;
    const EVALUATE_POISSONNOISE = 20;
    const EVALUATE_POW = 21;
    const EVALUATE_RIGHTSHIFT = 22;
    const EVALUATE_ROOT_MEAN_SQUARE = 23;
    const EVALUATE_SET = 24;
    const EVALUATE_SINE = 25;
    const EVALUATE_SUBTRACT = 26;
    const EVALUATE_SUM = 27;
    const EVALUATE_THRESHOLD = 29;
    const EVALUATE_THRESHOLDBLACK = 28;
    const EVALUATE_THRESHOLDWHITE = 30;
    const EVALUATE_UNDEFINED = 0;
    const EVALUATE_UNIFORMNOISE = 31;
    const EVALUATE_XOR = 32;
    const FILLRULE_EVENODD = 1;
    const FILLRULE_NONZERO = 2;
    const FILLRULE_UNDEFINED = 0;
    const FILTER_BARTLETT = 20;
    const FILTER_BESSEL = 13;
    const FILTER_BLACKMAN = 7;
    const FILTER_BOHMAN = 19;
    const FILTER_BOX = 2;
    const FILTER_CATROM = 11;
    const FILTER_COSINE = 28;
    const FILTER_CUBIC = 10;
    const FILTER_CUBIC_SPLINE = 31;
    const FILTER_GAUSSIAN = 8;
    const FILTER_HAMMING = 6;
    const FILTER_HANN = 5;
    const FILTER_HANNING = 5;
    const FILTER_HERMITE = 4;
    const FILTER_JINC = 13;
    const FILTER_KAISER = 16;
    const FILTER_LAGRANGE = 21;
    const FILTER_LANCZOS = 22;
    const FILTER_LANCZOS2 = 24;
    const FILTER_LANCZOS2SHARP = 25;
    const FILTER_LANCZOSRADIUS = 30;
    const FILTER_LANCZOSSHARP = 23;
    const FILTER_MITCHELL = 12;
    const FILTER_PARZEN = 18;
    const FILTER_POINT = 1;
    const FILTER_QUADRATIC = 9;
    const FILTER_ROBIDOUX = 26;
    const FILTER_ROBIDOUXSHARP = 27;
    const FILTER_SENTINEL = 32;
    const FILTER_SINC = 14;
    const FILTER_SINCFAST = 15;
    const FILTER_SPLINE = 29;
    const FILTER_TRIANGLE = 3;
    const FILTER_UNDEFINED = 0;
    const FILTER_WELCH = 17;
    const FILTER_WELSH = 17;
    const FUNCTION_ARCSIN = 1;
    const FUNCTION_ARCTAN = 2;
    const FUNCTION_POLYNOMIAL = 3;
    const FUNCTION_SINUSOID = 4;
    const FUNCTION_UNDEFINED = 0;
    const GRAVITY_CENTER = 5;
    const GRAVITY_EAST = 6;
    const GRAVITY_FORGET = 0;
    const GRAVITY_NORTH = 2;
    const GRAVITY_NORTHEAST = 3;
    const GRAVITY_NORTHWEST = 1;
    const GRAVITY_SOUTH = 8;
    const GRAVITY_SOUTHEAST = 9;
    const GRAVITY_SOUTHWEST = 7;
    const GRAVITY_WEST = 4;
    const IMAGE_TYPE_BILEVEL = 1;
    const IMAGE_TYPE_COLOR_SEPARATION = 8;
    const IMAGE_TYPE_COLOR_SEPARATION_ALPHA = 9;
    const IMAGE_TYPE_GRAYSCALE = 2;
    const IMAGE_TYPE_GRAYSCALE_ALPHA = 3;
    const IMAGE_TYPE_OPTIMIZE = 10;
    const IMAGE_TYPE_PALETTE = 4;
    const IMAGE_TYPE_PALETTE_ALPHA = 5;
    const IMAGE_TYPE_PALETTE_BILEVEL_ALPHA = 11;
    const IMAGE_TYPE_TRUE_COLOR = 6;
    const IMAGE_TYPE_TRUE_COLOR_ALPHA = 7;
    const IMAGICK_EXTNUM = 30700;
    const IMAGICK_EXTVER = '0.0.0';
    const IMGTYPE_BILEVEL = 1;
    const IMGTYPE_COLORSEPARATION = 8;
    const IMGTYPE_COLORSEPARATIONALPHA = 9;
    const IMGTYPE_COLORSEPARATIONMATTE = 9;
    const IMGTYPE_GRAYSCALE = 2;
    const IMGTYPE_GRAYSCALEALPHA = 3;
    const IMGTYPE_GRAYSCALEMATTE = 3;
    const IMGTYPE_OPTIMIZE = 10;
    const IMGTYPE_PALETTE = 4;
    const IMGTYPE_PALETTEALPHA = 5;
    const IMGTYPE_PALETTEBILEVELALPHA = 11;
    const IMGTYPE_PALETTEBILEVELMATTE = 11;
    const IMGTYPE_PALETTEMATTE = 5;
    const IMGTYPE_TRUECOLOR = 6;
    const IMGTYPE_TRUECOLORALPHA = 7;
    const IMGTYPE_TRUECOLORMATTE = 7;
    const IMGTYPE_UNDEFINED = 0;
    const INTERLACE_GIF = 5;
    const INTERLACE_JPEG = 6;
    const INTERLACE_LINE = 2;
    const INTERLACE_NO = 1;
    const INTERLACE_PARTITION = 4;
    const INTERLACE_PLANE = 3;
    const INTERLACE_PNG = 7;
    const INTERLACE_UNDEFINED = 0;
    const INTERPOLATE_AVERAGE = 1;
    const INTERPOLATE_AVERAGE_16 = 3;
    const INTERPOLATE_AVERAGE_9 = 2;
    const INTERPOLATE_BACKGROUND_COLOR = 4;
    const INTERPOLATE_BILINEAR = 5;
    const INTERPOLATE_BLEND = 6;
    const INTERPOLATE_CATROM = 7;
    const INTERPOLATE_INTEGER = 8;
    const INTERPOLATE_MESH = 9;
    const INTERPOLATE_NEAREST_PIXEL = 10;
    const INTERPOLATE_SPLINE = 11;
    const INTERPOLATE_UNDEFINED = 0;
    const KERNEL_BINOMIAL = 7;
    const KERNEL_BLUR = 5;
    const KERNEL_CHEBYSHEV = 33;
    const KERNEL_COMET = 6;
    const KERNEL_COMPASS = 13;
    const KERNEL_CONVEX_HULL = 30;
    const KERNEL_CORNERS = 25;
    const KERNEL_CROSS = 21;
    const KERNEL_DIAGONALS = 26;
    const KERNEL_DIAMOND = 15;
    const KERNEL_DIFFERENCE_OF_GAUSSIANS = 3;
    const KERNEL_DISK = 19;
    const KERNEL_EDGES = 24;
    const KERNEL_EUCLIDEAN = 36;
    const KERNEL_FREI_CHEN = 10;
    const KERNEL_GAUSSIAN = 2;
    const KERNEL_KIRSCH = 14;
    const KERNEL_LAPLACIAN = 8;
    const KERNEL_LAPLACIAN_OF_GAUSSIANS = 4;
    const KERNEL_LINE_ENDS = 27;
    const KERNEL_LINE_JUNCTIONS = 28;
    const KERNEL_MANHATTAN = 34;
    const KERNEL_OCTAGON = 18;
    const KERNEL_OCTAGONAL = 35;
    const KERNEL_PEAKS = 23;
    const KERNEL_PLUS = 20;
    const KERNEL_PREWITT = 12;
    const KERNEL_RECTANGLE = 17;
    const KERNEL_RIDGES = 29;
    const KERNEL_RING = 22;
    const KERNEL_ROBERTS = 11;
    const KERNEL_SKELETON = 32;
    const KERNEL_SOBEL = 9;
    const KERNEL_SQUARE = 16;
    const KERNEL_THIN_SE = 31;
    const KERNEL_UNITY = 1;
    const KERNEL_USER_DEFINED = 37;
    const LAYERMETHOD_COALESCE = 1;
    const LAYERMETHOD_COMPAREANY = 2;
    const LAYERMETHOD_COMPARECLEAR = 3;
    const LAYERMETHOD_COMPAREOVERLAY = 4;
    const LAYERMETHOD_COMPOSITE = 12;
    const LAYERMETHOD_DISPOSE = 5;
    const LAYERMETHOD_FLATTEN = 14;
    const LAYERMETHOD_MERGE = 13;
    const LAYERMETHOD_MOSAIC = 15;
    const LAYERMETHOD_OPTIMIZE = 6;
    const LAYERMETHOD_OPTIMIZEIMAGE = 7;
    const LAYERMETHOD_OPTIMIZEPLUS = 8;
    const LAYERMETHOD_OPTIMIZETRANS = 9;
    const LAYERMETHOD_REMOVEDUPS = 10;
    const LAYERMETHOD_REMOVEZERO = 11;
    const LAYERMETHOD_TRIMBOUNDS = 16;
    const LAYERMETHOD_UNDEFINED = 0;
    const LINECAP_BUTT = 1;
    const LINECAP_ROUND = 2;
    const LINECAP_SQUARE = 3;
    const LINECAP_UNDEFINED = 0;
    const LINEJOIN_BEVEL = 3;
    const LINEJOIN_MITER = 1;
    const LINEJOIN_ROUND = 2;
    const LINEJOIN_UNDEFINED = 0;
    const METRIC_ABSOLUTEERRORMETRIC = 1;
    const METRIC_FUZZERROR = 2;
    const METRIC_MEANABSOLUTEERROR = 3;
    const METRIC_MEANERRORPERPIXELMETRIC = 4;
    const METRIC_MEANSQUAREERROR = 5;
    const METRIC_NORMALIZEDCROSSCORRELATIONERRORMETRIC = 6;
    const METRIC_PEAKABSOLUTEERROR = 7;
    const METRIC_PEAKSIGNALTONOISERATIO = 8;
    const METRIC_PERCEPTUALHASH_ERROR = 9;
    const METRIC_ROOTMEANSQUAREDERROR = 10;
    const METRIC_STRUCTURAL_DISSIMILARITY_ERROR = 12;
    const METRIC_STRUCTURAL_SIMILARITY_ERROR = 11;
    const MONTAGEMODE_CONCATENATE = 3;
    const MONTAGEMODE_FRAME = 1;
    const MONTAGEMODE_UNFRAME = 2;
    const MORPHOLOGY_BOTTOM_HAT = 17;
    const MORPHOLOGY_CLOSE = 9;
    const MORPHOLOGY_CLOSE_INTENSITY = 11;
    const MORPHOLOGY_CONVOLVE = 1;
    const MORPHOLOGY_CORRELATE = 2;
    const MORPHOLOGY_DILATE = 4;
    const MORPHOLOGY_DILATE_INTENSITY = 6;
    const MORPHOLOGY_DISTANCE = 21;
    const MORPHOLOGY_EDGE = 15;
    const MORPHOLOGY_EDGE_IN = 13;
    const MORPHOLOGY_EDGE_OUT = 14;
    const MORPHOLOGY_ERODE = 3;
    const MORPHOLOGY_ERODE_INTENSITY = 5;
    const MORPHOLOGY_HIT_AND_MISS = 18;
    const MORPHOLOGY_ITERATIVE = 7;
    const MORPHOLOGY_OPEN = 8;
    const MORPHOLOGY_OPEN_INTENSITY = 10;
    const MORPHOLOGY_SMOOTH = 12;
    const MORPHOLOGY_THICKEN = 20;
    const MORPHOLOGY_THINNING = 19;
    const MORPHOLOGY_TOP_HAT = 16;
    const MORPHOLOGY_VORONOI = 22;
    const NOISE_GAUSSIAN = 2;
    const NOISE_IMPULSE = 4;
    const NOISE_LAPLACIAN = 5;
    const NOISE_MULTIPLICATIVEGAUSSIAN = 3;
    const NOISE_POISSON = 6;
    const NOISE_RANDOM = 7;
    const NOISE_UNIFORM = 1;
    const NORMALIZE_KERNEL_CORRELATE = 65536;
    const NORMALIZE_KERNEL_NONE = 0;
    const NORMALIZE_KERNEL_PERCENT = 4096;
    const NORMALIZE_KERNEL_VALUE = 8192;
    const ORIENTATION_BOTTOMLEFT = 4;
    const ORIENTATION_BOTTOMRIGHT = 3;
    const ORIENTATION_LEFTBOTTOM = 8;
    const ORIENTATION_LEFTTOP = 5;
    const ORIENTATION_RIGHTBOTTOM = 7;
    const ORIENTATION_RIGHTTOP = 6;
    const ORIENTATION_TOPLEFT = 1;
    const ORIENTATION_TOPRIGHT = 2;
    const ORIENTATION_UNDEFINED = 0;
    const PAINT_FILLTOBORDER = 4;
    const PAINT_FLOODFILL = 3;
    const PAINT_POINT = 1;
    const PAINT_REPLACE = 2;
    const PAINT_RESET = 5;
    const PATHUNITS_OBJECTBOUNDINGBOX = 3;
    const PATHUNITS_UNDEFINED = 0;
    const PATHUNITS_USERSPACE = 1;
    const PATHUNITS_USERSPACEONUSE = 2;
    const PIXELMASK_COMPOSITE = 4;
    const PIXELMASK_READ = 1;
    const PIXELMASK_WRITE = 2;
    const PIXELSTORAGE_CHAR = 1;
    const PIXELSTORAGE_DOUBLE = 2;
    const PIXELSTORAGE_FLOAT = 3;
    const PIXELSTORAGE_LONG = 4;
    const PIXELSTORAGE_QUANTUM = 6;
    const PIXELSTORAGE_SHORT = 7;
    const PIXEL_CHAR = 1;
    const PIXEL_DOUBLE = 2;
    const PIXEL_FLOAT = 3;
    const PIXEL_LONG = 4;
    const PIXEL_QUANTUM = 6;
    const PIXEL_SHORT = 7;
    const PREVIEW_ADDNOISE = 14;
    const PREVIEW_BLUR = 16;
    const PREVIEW_BRIGHTNESS = 6;
    const PREVIEW_CHARCOALDRAWING = 28;
    const PREVIEW_DESPECKLE = 12;
    const PREVIEW_DULL = 9;
    const PREVIEW_EDGEDETECT = 18;
    const PREVIEW_GAMMA = 7;
    const PREVIEW_GRAYSCALE = 10;
    const PREVIEW_HUE = 4;
    const PREVIEW_IMPLODE = 25;
    const PREVIEW_JPEG = 29;
    const PREVIEW_OILPAINT = 27;
    const PREVIEW_QUANTIZE = 11;
    const PREVIEW_RAISE = 22;
    const PREVIEW_REDUCENOISE = 13;
    const PREVIEW_ROLL = 3;
    const PREVIEW_ROTATE = 1;
    const PREVIEW_SATURATION = 5;
    const PREVIEW_SEGMENT = 23;
    const PREVIEW_SHADE = 21;
    const PREVIEW_SHARPEN = 15;
    const PREVIEW_SHEAR = 2;
    const PREVIEW_SOLARIZE = 20;
    const PREVIEW_SPIFF = 8;
    const PREVIEW_SPREAD = 19;
    const PREVIEW_SWIRL = 24;
    const PREVIEW_THRESHOLD = 17;
    const PREVIEW_UNDEFINED = 0;
    const PREVIEW_WAVE = 26;
    const QUANTUM_RANGE = 65535;
    const RENDERINGINTENT_ABSOLUTE = 3;
    const RENDERINGINTENT_PERCEPTUAL = 2;
    const RENDERINGINTENT_RELATIVE = 4;
    const RENDERINGINTENT_SATURATION = 1;
    const RENDERINGINTENT_UNDEFINED = 0;
    const RESOLUTION_PIXELSPERCENTIMETER = 2;
    const RESOLUTION_PIXELSPERINCH = 1;
    const RESOLUTION_UNDEFINED = 0;
    const RESOURCETYPE_AREA = 1;
    const RESOURCETYPE_DISK = 2;
    const RESOURCETYPE_FILE = 3;
    const RESOURCETYPE_HEIGHT = 4;
    const RESOURCETYPE_LISTLENGTH = 11;
    const RESOURCETYPE_MAP = 5;
    const RESOURCETYPE_MEMORY = 6;
    const RESOURCETYPE_THREAD = 7;
    const RESOURCETYPE_THROTTLE = 8;
    const RESOURCETYPE_TIME = 9;
    const RESOURCETYPE_UNDEFINED = 0;
    const RESOURCETYPE_WIDTH = 10;
    const SPARSECOLORMETHOD_BARYCENTRIC = 1;
    const SPARSECOLORMETHOD_BILINEAR = 7;
    const SPARSECOLORMETHOD_INVERSE = 19;
    const SPARSECOLORMETHOD_MANHATTAN = 20;
    const SPARSECOLORMETHOD_POLYNOMIAL = 8;
    const SPARSECOLORMETHOD_SPEPARDS = 16;
    const SPARSECOLORMETHOD_UNDEFINED = 0;
    const SPARSECOLORMETHOD_VORONOI = 18;
    const STATISTIC_CONTRAST = 10;
    const STATISTIC_GRADIENT = 1;
    const STATISTIC_MAXIMUM = 2;
    const STATISTIC_MEAN = 3;
    const STATISTIC_MEDIAN = 4;
    const STATISTIC_MINIMUM = 5;
    const STATISTIC_MODE = 6;
    const STATISTIC_NONPEAK = 7;
    const STATISTIC_ROOT_MEAN_SQUARE = 8;
    const STATISTIC_STANDARD_DEVIATION = 9;
    const STRETCH_ANY = 10;
    const STRETCH_CONDENSED = 4;
    const STRETCH_EXPANDED = 7;
    const STRETCH_EXTRACONDENSED = 3;
    const STRETCH_EXTRAEXPANDED = 8;
    const STRETCH_NORMAL = 1;
    const STRETCH_SEMICONDENSED = 5;
    const STRETCH_SEMIEXPANDED = 6;
    const STRETCH_ULTRACONDENSED = 2;
    const STRETCH_ULTRAEXPANDED = 9;
    const STYLE_ANY = 4;
    const STYLE_BOLD = 5;
    const STYLE_ITALIC = 2;
    const STYLE_NORMAL = 1;
    const STYLE_OBLIQUE = 3;
    const USE_ZEND_MM = 0;
    const VIRTUALPIXELMETHOD_BACKGROUND = 1;
    const VIRTUALPIXELMETHOD_BLACK = 9;
    const VIRTUALPIXELMETHOD_CHECKERTILE = 16;
    const VIRTUALPIXELMETHOD_DITHER = 2;
    const VIRTUALPIXELMETHOD_EDGE = 3;
    const VIRTUALPIXELMETHOD_GRAY = 10;
    const VIRTUALPIXELMETHOD_HORIZONTALTILE = 12;
    const VIRTUALPIXELMETHOD_HORIZONTALTILEEDGE = 14;
    const VIRTUALPIXELMETHOD_MASK = 8;
    const VIRTUALPIXELMETHOD_MIRROR = 4;
    const VIRTUALPIXELMETHOD_RANDOM = 5;
    const VIRTUALPIXELMETHOD_TILE = 6;
    const VIRTUALPIXELMETHOD_TRANSPARENT = 7;
    const VIRTUALPIXELMETHOD_UNDEFINED = 0;
    const VIRTUALPIXELMETHOD_VERTICALTILE = 13;
    const VIRTUALPIXELMETHOD_VERTICALTILEEDGE = 15;
    const VIRTUALPIXELMETHOD_WHITE = 11;

    public function __construct($files = null) { }
    public function adaptiveBlurImage(float $radius, float $sigma, int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }

    public function adaptiveResizeImage(
        int $columns,
        int $rows,
        bool $bestfit = false,
        bool $legacy = false
    ): bool { return false; }

    public function adaptiveSharpenImage(float $radius, float $sigma, int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }
    public function adaptiveThresholdImage(int $width, int $height, int $offset): bool { return false; }
    public function addImage(Imagick $source): bool { return false; }
    public function addNoiseImage(int $noise_type, int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }
    public function affineTransformImage(ImagickDraw $matrix): bool { return false; }
    public function animateImages(string $x_server): bool { return false; }

    public function annotateImage(
        ImagickDraw $draw_settings,
        float $x,
        float $y,
        float $angle,
        string $text
    ): bool { return false; }

    public function appendImages(bool $stack): Imagick { return new Imagick(); }
    public function autoLevelImage(int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }
    public function averageImages(): Imagick { return new Imagick(); }
    public function blackThresholdImage(mixed $threshold): bool { return false; }
    public function blueShiftImage(float $factor = 1.5): bool { return false; }
    public function blurImage(float $radius, float $sigma, int $channel = null): bool { return false; }
    public function borderImage(mixed $bordercolor, int $width, int $height): bool { return false; }
    public function brightnessContrastImage(float $brightness, float $contrast, int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }
    public function charcoalImage(float $radius, float $sigma): bool { return false; }

    public function chopImage(
        int $width,
        int $height,
        int $x,
        int $y
    ): bool { return false; }

    public function clampImage(int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }
    public function clear(): bool { return false; }
    public function clipImage(): bool { return false; }
    public function clipImagePath(string $pathname, string $inside): void { }
    public function clipPathImage(string $pathname, bool $inside): bool { return false; }
    public function clone(): Imagick { return new Imagick(); }
    public function clutImage(Imagick $lookup_table, int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }
    public function coalesceImages(): Imagick { return new Imagick(); }
    public function colorFloodfillImage(
        mixed $fill,
        float $fuzz,
        mixed $bordercolor,
        int $x,
        int $y
    ): bool { return false; }
    public function colorizeImage(mixed $colorize, mixed $opacity, bool $legacy = false): bool { return false; }
    public function colorMatrixImage(array $color_matrix = Imagick::CHANNEL_DEFAULT): bool { return false; }
    public function combineImages(int $channelType): Imagick { return new Imagick(); }
    public function commentImage(string $comment): bool { return false; }
    public function compareImageChannels(Imagick $image, int $channelType, int $metricType): array { return []; }
    public function compareImageLayers(int $method): Imagick { return new Imagick(); }
    public function compareImages(Imagick $compare, int $metric): array { return []; }

    public function compositeImage(
        Imagick $composite_object,
        int $composite,
        int $x,
        int $y,
        int $channel = Imagick::CHANNEL_DEFAULT
    ): bool { return false; }

    public function contrastImage(bool $sharpen): bool { return false; }
    public function contrastStretchImage(float $black_point, float $white_point, int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }
    public function convolveImage(array $kernel, int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }
    public function count(int $mode = 0): int { return 0; }

    public function cropImage(
        int $width,
        int $height,
        int $x,
        int $y
    ): bool { return false; }

    public function cropThumbnailImage(int $width, int $height, bool $legacy = false): bool { return false; }
    public function current(): Imagick { return new Imagick(); }
    public function cycleColormapImage(int $displace): bool { return false; }
    public function decipherImage(string $passphrase): bool { return false; }
    public function deconstructImages(): Imagick { return new Imagick(); }
    public function deleteImageArtifact(string $artifact): bool { return false; }
    public function deleteImageProperty(string $name): bool { return false; }
    public function deskewImage(float $threshold): bool { return false; }
    public function despeckleImage(): bool { return false; }
    public function destroy(): bool { return false; }
    public function displayImage(string $servername): bool { return false; }
    public function displayImages(string $servername): bool { return false; }
    public function distortImage(int $method, array $arguments, bool $bestfit): bool { return false; }
    public function drawImage(ImagickDraw $draw): bool { return false; }
    public function edgeImage(float $radius): bool { return false; }
    public function embossImage(float $radius, float $sigma): bool { return false; }
    public function encipherImage(string $passphrase): bool { return false; }
    public function enhanceImage(): bool { return false; }
    public function equalizeImage(): bool { return false; }
    public function evaluateImage(int $op, float $constant, int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }

    public function exportImagePixels(
        int $x,
        int $y,
        int $width,
        int $height,
        string $map,
        int $STORAGE
    ): array { return []; }

    public function extentImage(
        int $width,
        int $height,
        int $x,
        int $y
    ): bool { return false; }

    public function filter(ImagickKernel $ImagickKernel, int $channel = Imagick::CHANNEL_UNDEFINED): bool { return false; }
    public function flattenImages(): Imagick { return new Imagick(); }
    public function flipImage(): bool { return false; }

    public function floodFillPaintImage(
        mixed $fill,
        float $fuzz,
        mixed $target,
        int $x,
        int $y,
        bool $invert,
        int $channel = Imagick::CHANNEL_DEFAULT
    ): bool { return false; }

    public function flopImage(): bool { return false; }
    public function forwardFourierTransformimage(bool $magnitude): bool { return false; }

    public function frameImage(
        mixed $matte_color,
        int $width,
        int $height,
        int $inner_bevel,
        int $outer_bevel
    ): bool { return false; }

    public function functionImage(int $function, array $arguments, int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }
    public function fxImage(string $expression, int $channel = Imagick::CHANNEL_DEFAULT): Imagick { return new Imagick(); }
    public function gammaImage(float $gamma, int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }
    public function gaussianBlurImage(float $radius, float $sigma, int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }
    public function getColorspace(): int { return 0; }
    public function getCompression(): int { return 0; }
    public function getCompressionQuality(): int { return 0; }
    public static function getCopyright(): string { return 0; }
    public function getFilename(): string { return 0; }
    public function getFont(): string { return 0; }
    public function getFormat(): string { return 0; }
    public function getGravity(): int { return 0; }
    public static function getHomeURL(): string { return 0; }
    public function getImage(): Imagick { return new Imagick(); }
    public function getImageAlphaChannel(): bool { return false; }
    public function getImageArtifact(string $artifact): string { return 0; }
    public function getImageAttribute(string $key): string { return 0; }
    public function getImageBackgroundColor(): ImagickPixel { return new ImagickPixel(); }
    public function getImageBlob(): string { return 0; }
    public function getImageBluePrimary(): array { return []; }
    public function getImageBorderColor(): ImagickPixel { return new ImagickPixel(); }
    public function getImageChannelDepth(int $channel): int { return 0; }
    public function getImageChannelDistortion(Imagick $reference, int $channel, int $metric): float { return 0.0; }
    public function getImageChannelDistortions(Imagick $reference, int $metric, int $channel = Imagick::CHANNEL_DEFAULT): float { return 0.0; }
    public function getImageChannelExtrema(int $channel): array { return []; }
    public function getImageChannelKurtosis(int $channel = Imagick::CHANNEL_DEFAULT): array { return []; }
    public function getImageChannelMean(int $channel): array { return []; }
    public function getImageChannelRange(int $channel): array { return []; }
    public function getImageChannelStatistics(): array { return []; }
    public function getImageClipMask(): Imagick { return new Imagick(); }
    public function getImageColormapColor(int $index): ImagickPixel { return new ImagickPixel(); }
    public function getImageColors(): int { return 0; }
    public function getImageColorspace(): int { return 0; }
    public function getImageCompose(): int { return 0; }
    public function getImageCompression(): int { return 0; }
    public function getImageCompressionQuality(): int { return 0; }
    public function getImageDelay(): int { return 0; }
    public function getImageDepth(): int { return 0; }
    public function getImageDispose(): int { return 0; }
    public function getImageDistortion(MagickWand $reference, int $metric): float { return 0.0; }
    public function getImageExtrema(): array { return []; }
    public function getImageFilename(): string { return 0; }
    public function getImageFormat(): string { return 0; }
    public function getImageGamma(): float { return 0.0; }
    public function getImageGeometry(): array { return []; }
    public function getImageGravity(): int { return 0; }
    public function getImageGreenPrimary(): array { return []; }
    public function getImageHeight(): int { return 0; }
    public function getImageHistogram(): array { return []; }
    public function getImageIndex(): int { return 0; }
    public function getImageInterlaceScheme(): int { return 0; }
    public function getImageInterpolateMethod(): int { return 0; }
    public function getImageIterations(): int { return 0; }
    public function getImageLength(): int { return 0; }
    public function getImageMatte(): bool { return false; }
    public function getImageMatteColor(): ImagickPixel { return new ImagickPixel(); }
    public function getImageMimeType(): string { return 0; }
    public function getImageOrientation(): int { return 0; }
    public function getImagePage(): array { return []; }
    public function getImagePixelColor(int $x, int $y): ImagickPixel { return new ImagickPixel(); }
    public function getImageProfile(string $name): string { return 0; }
    public function getImageProfiles(string $pattern = "*", bool $include_values = true): array { return []; }
    public function getImageProperties(string $pattern = "*", bool $include_values = true): array { return []; }
    public function getImageProperty(string $name): string { return 0; }
    public function getImageRedPrimary(): array { return []; }

    public function getImageRegion(
        int $width,
        int $height,
        int $x,
        int $y
    ): Imagick { return new Imagick(); }

    public function getImageRenderingIntent(): int { return 0; }
    public function getImageResolution(): array { return []; }
    public function getImagesBlob(): string { return 0; }
    public function getImageScene(): int { return 0; }
    public function getImageSignature(): string { return 0; }
    public function getImageSize(): int { return 0; }
    public function getImageTicksPerSecond(): int { return 0; }
    public function getImageTotalInkDensity(): float { return 0.0; }
    public function getImageType(): int { return 0; }
    public function getImageUnits(): int { return 0; }
    public function getImageVirtualPixelMethod(): int { return 0; }
    public function getImageWhitePoint(): array { return []; }
    public function getImageWidth(): int { return 0; }
    public function getInterlaceScheme(): int { return 0; }
    public function getIteratorIndex(): int { return 0; }
    public function getNumberImages(): int { return 0; }
    public function getOption(string $key): string { return 0; }
    public static function getPackageName(): string { return 0; }
    public function getPage(): array { return []; }
    public function getPixelIterator(): ImagickPixel { return new ImagickPixel(); }

    public function getPixelRegionIterator(
        int $x,
        int $y,
        int $columns,
        int $rows
    ): ImagickPixel { return new ImagickPixel(); }

    public function getPointSize(): float { return 0.0; }
    public static function getQuantum(): int { return 0; }
    public static function getQuantumDepth(): array { return []; }
    public static function getQuantumRange(): array { return []; }
    public static function getRegistry(string $key): string { return 0; }
    public static function getReleaseDate(): string { return 0; }
    public static function getResource(int $type): int { return 0; }
    public static function getResourceLimit(int $type): int { return 0; }
    public function getSamplingFactors(): array { return []; }
    public function getSize(): array { return []; }
    public function getSizeOffset(): int { return 0; }
    public static function getVersion(): array { return []; }
    public function haldClutImage(Imagick $clut, int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }
    public function hasNextImage(): bool { return false; }
    public function hasPreviousImage(): bool { return false; }
    public function identifyFormat(string $embedText): string|false { return false; }
    public function identifyImage(bool $appendRawOutput = false): array { return []; }
    public function implodeImage(float $radius): bool { return false; }

    public function importImagePixels(
        int $x,
        int $y,
        int $width,
        int $height,
        string $map,
        int $storage,
        array $pixels
    ): bool { return false; }

    public function inverseFourierTransformImage(Imagick $complement, bool $magnitude): bool { return false; }
    public function labelImage(string $label): bool { return false; }

    public function levelImage(
        float $blackPoint,
        float $gamma,
        float $whitePoint,
        int $channel = Imagick::CHANNEL_DEFAULT
    ): bool { return false; }

    public function linearStretchImage(float $blackPoint, float $whitePoint): bool { return false; }

    public function liquidRescaleImage(
        int $width,
        int $height,
        float $delta_x,
        float $rigidity
    ): bool { return false; }

    public static function listRegistry(): array { return []; }
    public function magnifyImage(): bool { return false; }
    public function mapImage(Imagick $map, bool $dither): bool { return false; }

    public function matteFloodfillImage(
        float $alpha,
        float $fuzz,
        mixed $bordercolor,
        int $x,
        int $y
    ): bool { return false; }

    public function medianFilterImage(float $radius): bool { return false; }
    public function mergeImageLayers(int $layer_method): Imagick { return new Imagick(); }
    public function minifyImage(): bool { return false; }
    public function modulateImage(float $brightness, float $saturation, float $hue): bool { return false; }

    public function montageImage(
        ImagickDraw $draw,
        string $tile_geometry,
        string $thumbnail_geometry,
        int $mode,
        string $frame
    ): Imagick { return new Imagick(); }

    public function morphImages(int $number_frames): Imagick { return new Imagick(); }

    public function morphology(
        int $morphologyMethod,
        int $iterations,
        ImagickKernel $ImagickKernel,
        int $channel = Imagick::CHANNEL_DEFAULT
    ): bool { return false; }

    public function mosaicImages(): Imagick { return new Imagick(); }

    public function motionBlurImage(
        float $radius,
        float $sigma,
        float $angle,
        int $channel = Imagick::CHANNEL_DEFAULT
    ): bool { return false; }

    public function negateImage(bool $gray, int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }

    public function newImage(
        int $cols,
        int $rows,
        mixed $background,
        string $format = null
    ): bool { return false; }

    public function newPseudoImage(int $columns, int $rows, string $pseudoString): bool { return false; }
    public function nextImage(): bool { return false; }
    public function normalizeImage(int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }
    public function oilPaintImage(float $radius): bool { return false; }

    public function opaquePaintImage(
        mixed $target,
        mixed $fill,
        float $fuzz,
        bool $invert,
        int $channel = Imagick::CHANNEL_DEFAULT
    ): bool { return false; }

    public function optimizeImageLayers(): bool { return false; }
    public function orderedPosterizeImage(string $threshold_map, int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }

    public function paintFloodfillImage(
        mixed $fill,
        float $fuzz,
        mixed $bordercolor,
        int $x,
        int $y,
        int $channel = Imagick::CHANNEL_DEFAULT
    ): bool { return false; }

    public function paintOpaqueImage(
        mixed $target,
        mixed $fill,
        float $fuzz,
        int $channel = Imagick::CHANNEL_DEFAULT
    ): bool { return false; }

    public function paintTransparentImage(mixed $target, float $alpha, float $fuzz): bool { return false; }
    public function pingImage(string $filename): bool { return false; }
    public function pingImageBlob(string $image): bool { return false; }
    public function pingImageFile(resource $filehandle, string $fileName = null): bool { return false; }
    public function polaroidImage(ImagickDraw $properties, float $angle): bool { return false; }
    public function posterizeImage(int $levels, bool $dither): bool { return false; }
    public function previewImages(int $preview): bool { return false; }
    public function previousImage(): bool { return false; }
    public function profileImage(string $name, ?string $profile): bool { return false; }

    public function quantizeImage(
        int $numberColors,
        int $colorspace,
        int $treedepth,
        bool $dither,
        bool $measureError
    ): bool { return false; }

    public function quantizeImages(
        int $numberColors,
        int $colorspace,
        int $treedepth,
        bool $dither,
        bool $measureError
    ): bool { return false; }

    public function queryFontMetrics(ImagickDraw $properties, string $text, bool $multiline = null): array { return []; }
    public static function queryFonts(string $pattern = "*"): array { return []; }
    public static function queryFormats(string $pattern = "*"): array { return []; }
    public function radialBlurImage(float $angle, int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }

    public function raiseImage(
        int $width,
        int $height,
        int $x,
        int $y,
        bool $raise
    ): bool { return false; }

    public function randomThresholdImage(float $low, float $high, int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }
    public function readImage(string $filename): bool { return false; }
    public function readImageBlob(string $image, string $filename = null): bool { return false; }
    public function readImageFile(resource $filehandle, string $fileName = null): bool { return false; }
    public function readImages(array $filenames): bool { return false; }
    public function recolorImage(array $matrix): bool { return false; }
    public function reduceNoiseImage(float $radius): bool { return false; }
    public function remapImage(Imagick $replacement, int $DITHER): bool { return false; }
    public function removeImage(): bool { return false; }
    public function removeImageProfile(string $name): string { return 0; }
    public function render(): bool { return false; }

    public function resampleImage(
        float $x_resolution,
        float $y_resolution,
        int $filter,
        float $blur
    ): bool { return false; }

    public function resetImagePage(string $page): bool { return false; }

    public function resizeImage(
        int $columns,
        int $rows,
        int $filter,
        float $blur,
        bool $bestfit = false,
        bool $legacy = false
    ): bool { return false; }

    public function rollImage(int $x, int $y): bool { return false; }
    public function rotateImage(mixed $background, float $degrees): bool { return false; }
    public function rotationalBlurImage(float $angle, int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }

    public function roundCorners(
        float $x_rounding,
        float $y_rounding,
        float $stroke_width = 10,
        float $displace = 5,
        float $size_correction = -6
    ): bool { return false; }

    public function sampleImage(int $columns, int $rows): bool { return false; }

    public function scaleImage(
        int $columns,
        int $rows,
        bool $bestfit = false,
        bool $legacy = false
    ): bool { return false; }

    public function segmentImage(
        int $COLORSPACE,
        float $cluster_threshold,
        float $smooth_threshold,
        bool $verbose = false
    ): bool { return false; }

    public function selectiveBlurImage(
        float $radius,
        float $sigma,
        float $threshold,
        int $channel = Imagick::CHANNEL_DEFAULT
    ): bool { return false; }

    public function separateImageChannel(int $channel): bool { return false; }
    public function sepiaToneImage(float $threshold): bool { return false; }
    public function setBackgroundColor(mixed $background): bool { return false; }
    public function setColorspace(int $COLORSPACE): bool { return false; }
    public function setCompression(int $compression): bool { return false; }
    public function setCompressionQuality(int $quality): bool { return false; }
    public function setFilename(string $filename): bool { return false; }
    public function setFirstIterator(): bool { return false; }
    public function setFont(string $font): bool { return false; }
    public function setFormat(string $format): bool { return false; }
    public function setGravity(int $gravity): bool { return false; }
    public function setImage(Imagick $replace): bool { return false; }
    public function setImageAlphaChannel(int $mode): bool { return false; }
    public function setImageArtifact(string $artifact, string $value): bool { return false; }
    public function setImageAttribute(string $key, string $value): bool { return false; }
    public function setImageBackgroundColor(mixed $background): bool { return false; }
    public function setImageBias(float $bias): bool { return false; }
    public function setImageBiasQuantum(float $bias): void { }
    public function setImageBluePrimary(float $x, float $y): bool { return false; }
    public function setImageBorderColor(mixed $border): bool { return false; }
    public function setImageChannelDepth(int $channel, int $depth): bool { return false; }
    public function setImageClipMask(Imagick $clip_mask): bool { return false; }
    public function setImageColormapColor(int $index, ImagickPixel $color): bool { return false; }
    public function setImageColorspace(int $colorspace): bool { return false; }
    public function setImageCompose(int $compose): bool { return false; }
    public function setImageCompression(int $compression): bool { return false; }
    public function setImageCompressionQuality(int $quality): bool { return false; }
    public function setImageDelay(int $delay): bool { return false; }
    public function setImageDepth(int $depth): bool { return false; }
    public function setImageDispose(int $dispose): bool { return false; }
    public function setImageExtent(int $columns, int $rows): bool { return false; }
    public function setImageFilename(string $filename): bool { return false; }
    public function setImageFormat(string $format): bool { return false; }
    public function setImageGamma(float $gamma): bool { return false; }
    public function setImageGravity(int $gravity): bool { return false; }
    public function setImageGreenPrimary(float $x, float $y): bool { return false; }
    public function setImageIndex(int $index): bool { return false; }
    public function setImageInterlaceScheme(int $interlace_scheme): bool { return false; }
    public function setImageInterpolateMethod(int $method): bool { return false; }
    public function setImageIterations(int $iterations): bool { return false; }
    public function setImageMatte(bool $matte): bool { return false; }
    public function setImageMatteColor(mixed $matte): bool { return false; }
    public function setImageOpacity(float $opacity): bool { return false; }
    public function setImageOrientation(int $orientation): bool { return false; }

    public function setImagePage(
        int $width,
        int $height,
        int $x,
        int $y
    ): bool { return false; }

    public function setImageProfile(string $name, string $profile): bool { return false; }
    public function setImageProperty(string $name, string $value): bool { return false; }
    public function setImageRedPrimary(float $x, float $y): bool { return false; }
    public function setImageRenderingIntent(int $rendering_intent): bool { return false; }
    public function setImageResolution(float $x_resolution, float $y_resolution): bool { return false; }
    public function setImageScene(int $scene): bool { return false; }
    public function setImageTicksPerSecond(int $ticks_per_second): bool { return false; }
    public function setImageType(int $image_type): bool { return false; }
    public function setImageUnits(int $units): bool { return false; }
    public function setImageVirtualPixelMethod(int $method): bool { return false; }
    public function setImageWhitePoint(float $x, float $y): bool { return false; }
    public function setInterlaceScheme(int $interlace_scheme): bool { return false; }
    public function setIteratorIndex(int $index): bool { return false; }
    public function setLastIterator(): bool { return false; }
    public function setOption(string $key, string $value): bool { return false; }

    public function setPage(
        int $width,
        int $height,
        int $x,
        int $y
    ): bool { return false; }

    public function setPointSize(float $point_size): bool { return false; }
    public function setProgressMonitor(callable $callback): bool { return false; }
    public static function setRegistry(string $key, string $value): bool { return false; }
    public function setResolution(float $x_resolution, float $y_resolution): bool { return false; }
    public static function setResourceLimit(int $type, int $limit): bool { return false; }
    public function setSamplingFactors(array $factors): bool { return false; }
    public function setSize(int $columns, int $rows): bool { return false; }
    public function setSizeOffset(int $columns, int $rows, int $offset): bool { return false; }
    public function setType(int $image_type): bool { return false; }
    public function shadeImage(bool $gray, float $azimuth, float $elevation): bool { return false; }

    public function shadowImage(
        float $opacity,
        float $sigma,
        int $x,
        int $y
    ): bool { return false; }

    public function sharpenImage(float $radius, float $sigma, int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }
    public function shaveImage(int $columns, int $rows): bool { return false; }
    public function shearImage(mixed $background, float $x_shear, float $y_shear): bool { return false; }

    public function sigmoidalContrastImage(
        bool $sharpen,
        float $alpha,
        float $beta,
        int $channel = Imagick::CHANNEL_DEFAULT
    ): bool { return false; }

    public function sketchImage(float $radius, float $sigma, float $angle): bool { return false; }
    public function smushImages(bool $stack, int $offset): Imagick { return new Imagick(); }
    public function solarizeImage(int $threshold): bool { return false; }
    public function sparseColorImage(int $SPARSE_METHOD, array $arguments, int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }

    public function spliceImage(
        int $width,
        int $height,
        int $x,
        int $y
    ): bool { return false; }

    public function spreadImage(float $radius): bool { return false; }

    public function statisticImage(
        int $type,
        int $width,
        int $height,
        int $channel = Imagick::CHANNEL_DEFAULT
    ): bool { return false; }

    public function steganoImage(Imagick $watermark_wand, int $offset): Imagick { return new Imagick(); }
    public function stereoImage(Imagick $offset_wand): bool { return false; }
    public function stripImage(): bool { return false; }
    public function subImageMatch(Imagick $Imagick, array &$offset = null, float &$similarity = null): Imagick { return new Imagick(); }
    public function swirlImage(float $degrees): bool { return false; }
    public function textureImage(Imagick $texture_wand): Imagick { return new Imagick(); }
    public function thresholdImage(float $threshold, int $channel = Imagick::CHANNEL_DEFAULT): bool { return false; }

    public function thumbnailImage(
        int $columns,
        int $rows,
        bool $bestfit = false,
        bool $fill = false,
        bool $legacy = false
    ): bool { return false; }

    public function tintImage(mixed $tint, mixed $opacity, bool $legacy = false): bool { return false; }
    public function __toString(): string { return 0; }
    public function transformImage(string $crop, string $geometry): Imagick { return new Imagick(); }
    public function transformImageColorspace(int $colorspace): bool { return false; }

    public function transparentPaintImage(
        mixed $target,
        float $alpha,
        float $fuzz,
        bool $invert
    ): bool { return false; }

    public function transposeImage(): bool { return false; }
    public function transverseImage(): bool { return false; }
    public function trimImage(float $fuzz): bool { return false; }
    public function uniqueImageColors(): bool { return false; }

    public function unsharpMaskImage(
        float $radius,
        float $sigma,
        float $amount,
        float $threshold,
        int $channel = Imagick::CHANNEL_DEFAULT
    ): bool { return false; }

    public function valid(): bool { return false; }

    public function vignetteImage(
        float $blackPoint,
        float $whitePoint,
        int $x,
        int $y
    ): bool { return false; }

    public function waveImage(float $amplitude, float $length): bool { return false; }
    public function whiteThresholdImage(mixed $threshold): bool { return false; }
    public function writeImage(string $filename = NULL): bool { return false; }
    public function writeImageFile(resource $filehandle, string $format = null): bool { return false; }
    public function writeImages(string $filename, bool $adjoin): bool { return false; }
    public function writeImagesFile(resource $filehandle, string $format = null): bool { return false; }
}
