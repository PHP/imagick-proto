<?php

class ImagickDraw {
    public function affine(array $affine): bool { return false; }
    public function annotation(float $x, float $y, string $text): bool { return false; }
    public function arc(
        float $sx,
        float $sy,
        float $ex,
        float $ey,
        float $sd,
        float $ed
    ): bool { return false; }
    public function bezier(array $coordinates): bool { return false; }
    public function circle(
        float $ox,
        float $oy,
        float $px,
        float $py
    ): bool { return false; }
    public function clear(): bool { return false; }
    public function clone(): ImagickDraw { return new ImagickDraw(); }
    public function color(float $x, float $y, int $paintMethod): bool { return false; }
    public function comment(string $comment): bool { return false; }
    public function composite(
        int $compose,
        float $x,
        float $y,
        float $width,
        float $height,
        Imagick $compositeWand
    ): bool { return false; }
    public function __construct() { }
    public function destroy(): bool { return false; }
    public function ellipse(
        float $ox,
        float $oy,
        float $rx,
        float $ry,
        float $start,
        float $end
    ): bool { return false; }
    public function getClipPath(): string { return ''; }
    public function getClipRule(): int { return 0; }
    public function getClipUnits(): int { return 0; }
    public function getFillColor(): ImagickPixel { return new ImagickPixel(); }
    public function getFillOpacity(): float { return 0.0; }
    public function getFillRule(): int { return 0; }
    public function getFont(): string { return ''; }
    public function getFontFamily(): string { return ''; }
    public function getFontSize(): float { return 0.0; }
    public function getFontStretch(): int { return 0; }
    public function getFontStyle(): int { return 0; }
    public function getFontWeight(): int { return 0; }
    public function getGravity(): int { return 0; }
    public function getStrokeAntialias(): bool { return false; }
    public function getStrokeColor(): ImagickPixel { return new ImagickPixel(); }
    public function getStrokeDashArray(): array { return []; }
    public function getStrokeDashOffset(): float { return 0.0; }
    public function getStrokeLineCap(): int { return 0; }
    public function getStrokeLineJoin(): int { return 0; }
    public function getStrokeMiterLimit(): int { return 0; }
    public function getStrokeOpacity(): float { return 0.0; }
    public function getStrokeWidth(): float { return 0.0; }
    public function getTextAlignment(): int { return 0; }
    public function getTextAntialias(): bool { return false; }
    public function getTextDecoration(): int { return 0; }
    public function getTextEncoding(): string { return ''; }
    public function getTextInterlineSpacing(): float { return 0.0; }
    public function getTextInterwordSpacing(): float { return 0.0; }
    public function getTextKerning(): float { return 0.0; }
    public function getTextUnderColor(): ImagickPixel { return new ImagickPixel(); }
    public function getVectorGraphics(): string { return ''; }
    public function line(
        float $sx,
        float $sy,
        float $ex,
        float $ey
    ): bool { return false; }
    public function matte(float $x, float $y, int $paintMethod): bool { return false; }
    public function pathClose(): bool { return false; }
    public function pathCurveToAbsolute(
        float $x1,
        float $y1,
        float $x2,
        float $y2,
        float $x,
        float $y
    ): bool { return false; }
    public function pathCurveToQuadraticBezierAbsolute(
        float $x1,
        float $y1,
        float $x,
        float $y
    ): bool { return false; }
    public function pathCurveToQuadraticBezierRelative(
        float $x1,
        float $y1,
        float $x,
        float $y
    ): bool { return false; }
    public function pathCurveToQuadraticBezierSmoothAbsolute(float $x, float $y): bool { return false; }
    public function pathCurveToQuadraticBezierSmoothRelative(float $x, float $y): bool { return false; }
    public function pathCurveToRelative(
        float $x1,
        float $y1,
        float $x2,
        float $y2,
        float $x,
        float $y
    ): bool { return false; }
    public function pathCurveToSmoothAbsolute(
        float $x2,
        float $y2,
        float $x,
        float $y
    ): bool { return false; }
    public function pathCurveToSmoothRelative(
        float $x2,
        float $y2,
        float $x,
        float $y
    ): bool { return false; }
    public function pathEllipticArcAbsolute(
        float $rx,
        float $ry,
        float $x_axis_rotation,
        bool $large_arc_flag,
        bool $sweep_flag,
        float $x,
        float $y
    ): bool { return false; }
    public function pathEllipticArcRelative(
        float $rx,
        float $ry,
        float $x_axis_rotation,
        bool $large_arc_flag,
        bool $sweep_flag,
        float $x,
        float $y
    ): bool { return false; }
    public function pathFinish(): bool { return false; }
    public function pathLineToAbsolute(float $x, float $y): bool { return false; }
    public function pathLineToHorizontalAbsolute(float $x): bool { return false; }
    public function pathLineToHorizontalRelative(float $x): bool { return false; }
    public function pathLineToRelative(float $x, float $y): bool { return false; }
    public function pathLineToVerticalAbsolute(float $y): bool { return false; }
    public function pathLineToVerticalRelative(float $y): bool { return false; }
    public function pathMoveToAbsolute(float $x, float $y): bool { return false; }
    public function pathMoveToRelative(float $x, float $y): bool { return false; }
    public function pathStart(): bool { return false; }
    public function point(float $x, float $y): bool { return false; }
    public function polygon(array $coordinates): bool { return false; }
    public function polyline(array $coordinates): bool { return false; }
    public function pop(): bool { return false; }
    public function popClipPath(): bool { return false; }
    public function popDefs(): bool { return false; }
    public function popPattern(): bool { return false; }
    public function push(): bool { return false; }
    public function pushClipPath(string $clip_mask_id): bool { return false; }
    public function pushDefs(): bool { return false; }
    public function pushPattern(
        string $pattern_id,
        float $x,
        float $y,
        float $width,
        float $height
    ): bool { return false; }
    public function rectangle(
        float $x1,
        float $y1,
        float $x2,
        float $y2
    ): bool { return false; }
    public function render(): bool { return false; }
    public function resetVectorGraphics(): bool { return false; }
    public function rotate(float $degrees): bool { return false; }
    public function roundRectangle(
        float $x1,
        float $y1,
        float $x2,
        float $y2,
        float $rx,
        float $ry
    ): bool { return false; }
    public function scale(float $x, float $y): bool { return false; }
    public function setClipPath(string $clip_mask): bool { return false; }
    public function setClipRule(int $fill_rule): bool { return false; }
    public function setClipUnits(int $clip_units): bool { return false; }
    public function setFillAlpha(float $opacity): bool { return false; }
    public function setFillColor(ImagickPixel $fill_pixel): bool { return false; }
    public function setFillOpacity(float $fillOpacity): bool { return false; }
    public function setFillPatternURL(string $fill_url): bool { return false; }
    public function setFillRule(int $fill_rule): bool { return false; }
    public function setFont(string $font_name): bool { return false; }
    public function setFontFamily(string $font_family): bool { return false; }
    public function setFontSize(float $pointsize): bool { return false; }
    public function setFontStretch(int $fontStretch): bool { return false; }
    public function setFontStyle(int $style): bool { return false; }
    public function setFontWeight(int $font_weight): bool { return false; }
    public function setGravity(int $gravity): bool { return false; }
    public function setResolution(float $x_resolution, float $y_resolution): bool { return false; }
    public function setStrokeAlpha(float $opacity): bool { return false; }
    public function setStrokeAntialias(bool $stroke_antialias): bool { return false; }
    public function setStrokeColor(ImagickPixel $stroke_pixel): bool { return false; }
    public function setStrokeDashArray(array $dashArray): bool { return false; }
    public function setStrokeDashOffset(float $dash_offset): bool { return false; }
    public function setStrokeLineCap(int $linecap): bool { return false; }
    public function setStrokeLineJoin(int $linejoin): bool { return false; }
    public function setStrokeMiterLimit(int $miterlimit): bool { return false; }
    public function setStrokeOpacity(float $stroke_opacity): bool { return false; }
    public function setStrokePatternURL(string $stroke_url): bool { return false; }
    public function setStrokeWidth(float $stroke_width): bool { return false; }
    public function setTextAlignment(int $alignment): bool { return false; }
    public function setTextAntialias(bool $antiAlias): bool { return false; }
    public function setTextDecoration(int $decoration): bool { return false; }
    public function setTextEncoding(string $encoding): bool { return false; }
    public function setTextInterlineSpacing(float $spacing): bool { return false; }
    public function setTextInterwordSpacing(float $spacing): bool { return false; }
    public function setTextKerning(float $kerning): bool { return false; }
    public function setTextUnderColor(ImagickPixel $under_color): bool { return false; }
    public function setVectorGraphics(string $xml): bool { return false; }
    public function setViewbox(
        int $x1,
        int $y1,
        int $x2,
        int $y2
    ): bool { return false; }
    public function skewX(float $degrees): bool { return false; }
    public function skewY(float $degrees): bool { return false; }
    public function translate(float $x, float $y): bool { return false; }
}
