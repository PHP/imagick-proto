<?php

class ImagickPixel {
    public function clear(): bool { return false; }
    public function __construct(string $color = null) { }
    public function destroy(): bool { return false; }
    public function getColor(int $normalized = 0): array { return []; }
    public function getColorAsString(): string { return ''; }
    public function getColorCount(): int { return 0; }
    public function getColorQuantum(): array { return []; }
    public function getColorValue(int $color): float { return 0.0; }
    public function getColorValueQuantum(int $color): int|float { return 0; }
    public function getHSL(): array { return []; }
    public function getIndex(): int { return 0; }
    public function isPixelSimilar(ImagickPixel $color, float $fuzz): bool { return false; }
    public function isPixelSimilarQuantum(string $color, string $fuzz = null): bool { return false; }
    public function isSimilar(ImagickPixel $color, float $fuzz): bool { return false; }
    public function setColor(string $color): bool { return false; }
    public function setcolorcount(int $colorCount): bool { return false; }
    public function setColorValue(int $color, float $value): bool { return false; }
    public function setColorValueQuantum(int $color, int|float $value): bool { return false; }
    public function setHSL(float $hue, float $saturation, float $luminosity): bool { return false; }
    public function setIndex(int $index): bool { return false; }
}
